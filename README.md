# nextcloud Ansible role

Installs and configures Nextcloud.

Installs Nextcloud, MySQL, Apache and Redis on Debian/Ubuntu.

You may need to perform additional configuration using Nextcloud's web
interface. See section "Limitations" below for that.

## Parameters

### nextcloud_fqdn

The domain name for Nextcloud.

### nextcloud_admin_user

The username for the Nextcloud administrator. The default is `admin`.

### nextcloud_admin_user_password

The Nextcloud administrator password. This will usually be in the vault.

### php_memory_limit

Default 512M.

### opcache_interned_strings_buffer

The default for this PHP setting is 8. However in some installations it
might need to be set to 16, and sometimes to 32. It seems to depend on
the installed apps. See the [related support forum
discussion](https://help.nextcloud.com/t/nextcloud-23-02-opcache-interned-strings-buffer/134007/4)
for more information.

### default_phone_region

A country code like "GR". There is no default. This is used for
Nextcloud's `default_phone_region` configuration parameter.

### mail_from_address, mail_domain

The email address from which email notifications from Nextcloud appear
to be sent. For example, to use `noreply@example.com`, specify
`mail_from_address=noreply` and `mail_domain=example.com`.

These settings are those that can be set in the web interface, under
Basic settings, Email server. This role will overwrite these settings
whenever Ansible is run.

It will always use localhost port 25 as the smarthost, without
authentication and without encryption. For this to work, use Ansible
role `mail-satellite`.

### mysql_client_key, mysql_client_cert, mysql_ca_cert

By default, these parameters are empty. In this case, Nextcloud connects
to MySQL without TLS. If they have values, they must be pathnames (in
the Ansible controller) from which the keys and certificates are taken
and installed in the Nextcloud server. In this case, Nextcloud is
configured to connect to MySQL with TLS.

Either all three must be empty, or all three must have a value.

## Limitations

### Server setup

Many things are hardwired. The current assumption is that Nextcloud,
MySQL, Redis and Apache are all going to be in the same machine.

### Setting up theming

It seems to be nontrivial to setup theming through the command line,
particularly to setup logo, background and favicon. Therefore, the role
does not touch theming; use the web interface to setup theming after
Ansible is run.

### Setting up the Mail app

It doesn't seem to be possible to setup the Mail app through the command
line or Ansible. You need to go to the web interface, logon as admin,
and go to Settings, Administration, Groupware.

## Meta

Written by Antonis Christofides.

Copyright (C) 2022-2023 GRNET

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
