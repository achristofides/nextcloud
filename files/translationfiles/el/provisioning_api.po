# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Nextcloud package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# ΦΩΤΕΙΝΗ ΧΑΛΚΙΑ <photchalkia@gmail.com>, 2021
# Efstathios Iosifidis <iefstathios@gmail.com>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Nextcloud 3.14159\n"
"Report-Msgid-Bugs-To: translations\\@example.com\n"
"POT-Creation-Date: 2022-03-31 02:14+0000\n"
"PO-Revision-Date: 2020-09-18 18:53+0000\n"
"Last-Translator: Efstathios Iosifidis <iefstathios@gmail.com>, 2021\n"
"Language-Team: Greek (https://www.transifex.com/nextcloud/teams/64236/el/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: el\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: /app/apps/provisioning_api/lib/Controller/AppConfigController.php:143
msgid ""
"Logged in user must be an administrator or have authorization to edit this "
"setting."
msgstr ""

#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:89
msgid "Email confirmation"
msgstr "Επιβεβαίωση ηλεκτρονικού ταχυδρομείου"

#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:90
#, php-format
msgid "To enable the email address %s please click the button below."
msgstr ""
"Για να ενεργοποιήσετε τη διεύθυνση ηλεκτρονικού ταχυδρομείου %s κάντε κλικ "
"στο παρακάτω κουμπί."

#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:91
msgid "Confirm"
msgstr "Επιβεβαίωση"

#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:115
msgid ""
"Email was already removed from account and cannot be confirmed anymore."
msgstr ""
"Η διεύθυνση ηλεκτρονικού ταχυδρομείου έχει ήδη αφαιρεθεί από τον λογαριασμό "
"και δεν μπορεί πλέον να επιβεβαιωθεί."

#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:122
msgid "Could not verify mail because the token is expired."
msgstr ""
"Δεν ήταν δυνατή η επαλήθευση της αλληλογραφίας επειδή το διακριτικό έχει "
"λήξει."

#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:123
msgid "Could not verify mail because the token is invalid."
msgstr ""
"Αδυναμία επαλήθευσης ηλεκτρονικής αλληλογραφίας επειδή το διακριτικό δεν "
"είναι έγκυρο."

#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:127
msgid "An unexpected error occurred. Please contact your admin."
msgstr ""

#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:139
#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:140
msgid "Email confirmation successful"
msgstr "Η επιβεβαίωση μέσω ηλεκτρονικού ταχυδρομείου ήταν επιτυχής"

#: /app/apps/provisioning_api/specialAppInfoFakeDummyForL10nScript.php:2
msgid "Provisioning API"
msgstr "API παροχής"

#: /app/apps/provisioning_api/specialAppInfoFakeDummyForL10nScript.php:3
msgid ""
"This application enables a set of APIs that external systems can use to "
"manage users, groups and apps."
msgstr ""
"Αυτή η εφαρμογή επιτρέπει ένα σύνολο API που μπορούν να χρησιμοποιήσουν τα "
"εξωτερικά συστήματα για τη διαχείριση χρηστών, ομάδων και εφαρμογών."

#: /app/apps/provisioning_api/specialAppInfoFakeDummyForL10nScript.php:4
msgid ""
"This application enables a set of APIs that external systems can use to create, edit, delete and query user\n"
"\t\tattributes, query, set and remove groups, set quota and query total storage used in Nextcloud. Group admin users\n"
"\t\tcan also query Nextcloud and perform the same functions as an admin for groups they manage. The API also enables\n"
"\t\tan admin to query for active Nextcloud applications, application info, and to enable or disable an app remotely.\n"
"\t\tOnce the app is enabled, HTTP requests can be used via a Basic Auth header to perform any of the functions\n"
"\t\tlisted above. More information is available in the Provisioning API documentation, including example calls\n"
"\t\tand server responses."
msgstr ""
"Αυτή η εφαρμογή επιτρέπει ένα σύνολο API που μπορούν να χρησιμοποιήσουν τα "
"εξωτερικά συστήματα για να δημιουργήσουν, να επεξεργαστούν, να διαγράψουν "
"και να ρωτήσουν τις ιδιότητες των χρηστών, το ερώτημα, να ορίσουν και να "
"αφαιρέσουν ομάδες, να ορίσουν το όριο και το συνολικό χώρο αποθήκευσης "
"ερωτημάτων που χρησιμοποιούνται στο Nextcloud. Οι χρήστες διαχειριστή ομάδας"
" μπορούν επίσης να υποβάλουν ερώτημα στο Nextcloud και να εκτελούν τις ίδιες"
" λειτουργίες με έναν διαχειριστή για ομάδες που αυτοί διαχειρίζονται. Το API"
" επιτρέπει επίσης σε έναν διαχειριστή να υποβάλλει ερώτημα για ενεργές "
"εφαρμογές Nextcloud, πληροφορίες εφαρμογής και να ενεργοποιήσει ή να "
"απενεργοποιήσει μια εφαρμογή από απόσταση. Μόλις ενεργοποιηθεί η εφαρμογή, "
"τα αιτήματα HTTP μπορούν να χρησιμοποιηθούν μέσω κεφαλίδας Basic Auth για να"
" εκτελέσουν οποιαδήποτε των λειτουργιών που αναφέρονται παραπάνω. "
"Περισσότερες πληροφορίες διατίθενται στην τεκμηρίωση Provisioning API, "
"συμπεριλαμβανομένων παραδειγμάτων κλήσεων και απαντήσεων διακομιστή."

#: /app/apps/provisioning_api/lib/Controller/VerificationController.php:127
msgid "An unexpected error occurred. Please consult your sysadmin."
msgstr ""
"Προέκυψε ένα μη αναμενόμενο σφάλμα. Παρακαλούμε συμβουλευτείτε τον "
"διαχειριστή σας."
